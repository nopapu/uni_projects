Module -> Concurrent and Parallel Systems

--------------------------------------------------------------
NBody.dox explains the project and 
				displays the results obtained
--------------------------------------------------------------

Student -> STU40127794, Noe Pages Puertas


Double click in the CourseWork_Part2 Visual Studio Icon.



::::FINAL VERSION:


*03_Parallel_N_Body:	Parallelisation of the n body using shared memory



Recommended Solution Configurations:

Select 'Release' in the solution configurations.
Click in the Debug menu and select 'start without debugging'





:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
::Other projects inside CourseWork_Part2::


*01_Basic_N_Body:	A Mathematical n-body integration is tested in this program.

*02_Graphical_N_Body:	A Graphical, n-body system is displayed in this program. It has an Open MP optimisation.

*02_SIMDTestingGround: 	is the project where the operations with SIMD have been tested.

*03_Parallel_N_Body:	The GPU parallel, n-body simulation can be tested in this program.

*libENUgraphics 	Render Framework provided by Dr. Kevin Chalmers. Edinburgh Napier University.

*test_01_CUDA:		Test in which 2 lists of glm::vec4 are added and copied into another buffer.

*test_02_CUDA:		Test that ports in a simple fashion the algorithm that integrates the n-body.

*test_03_CUDA:		Test to implement the algorithm shown in CUDA gems3.

*test_04_CUDA:		This test has been carried out with the intention of separing and knitting cu and cuh files.
