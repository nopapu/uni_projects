/*c
........................................................
Name:		n-BodyManagerPar.cu
Details:	cu file where the methods for nbody for GPU parallelisation are developed.
			kernels have to be separate from the struct.
........................................................
Module:		Concurrent and parallel systems.
Author:		Noe Pages Puertas.
Date:		01/12/2014 | Napier University | 40127794@live.napier.ac.uk
*/

#include "n-BodyManagerPar.cuh"

// Constructors
NBodyPar::NBodyPar(){};

NBodyPar::NBodyPar(int Data_Size, float Gravity, float SoftFactor, int ELEMENTS)
: data_size(Data_Size), G(Gravity), softFactor(SoftFactor), elements(ELEMENTS)
{}

// Struct methods
// Initialise allocates memory
void NBodyPar::Initialise()
{
	cudaSetDevice(0);

	cudaMalloc((void**)&buffPos, data_size);
	cudaMalloc((void**)&buffForce, data_size);
}

vector<glm::vec4> NBodyPar::Update_N_Body(vector<glm::vec4> CPUbuffin, vector<glm::vec4> CPUbuffout)
{
	// clears the buffer
	results.clear();
	cudaSetDevice(0);

	// Memory is read from the CPU and copied to the GPU
	cudaMemcpy(buffPos, &CPUbuffin[0], data_size, cudaMemcpyHostToDevice);

	// matches with const NBODYSIZE global in the main03 file
	int blocks = 8;
	int threads = 500;
	
	
	// ::- > global kernel call
	// Grid(number of blocks, number of threads) + required size of shared memory per block
	CalculateForces << < blocks, threads, sizeof(glm::vec4) * elements / blocks >> >
		// kernel arguments
		(buffPos, buffForce, blocks, G, softFactor);

	// Wait for kernel to complete
	cudaDeviceSynchronize();

	// resizes the vector
	results.resize(elements);
	
	// Memory is copied back to the CPU
	cudaMemcpy(&results[0], buffForce, data_size, cudaMemcpyDeviceToHost);

	//cudaDeviceSynchronize();
	return results;
}

/*

CUDA Methods.

*/

// More in depth explanation in the link below
// http://www.3dgep.com/cuda-case-study-n-body-simulation/

// Calculation of the influence of particle i over particle j
__device__ glm::vec4 PartPartAttraction(glm::vec4 myPosition, glm::vec4 pj, glm::vec4 force, float G, float softFactor)
{
	// distance from part i to part j
	glm::vec4 r = pj - myPosition;

	// n-body equation
	force = ((G * (myPosition.w * pj.w) * r) /
		glm::pow(glm::length(r) * glm::length(r) + softFactor, 0.6666666666666667f));

	return force;
}

// The for loop that calculates the influence of the positions in a tile on myPosition.
__device__ glm::vec4 TileCalculation(glm::vec4 myPosition, glm::vec4 force, float G, float softFactor)
{
	extern __shared__ glm::vec4 shPosition[];
	
	// We create the vector to accumulate the forces
	glm::vec4 newforce(0, 0, 0, 1.0f);
	
	for (int i = 0; i < blockDim.x; i++)
	{
		// This is the way to accumulate forces through the for loop.
		newforce += PartPartAttraction(myPosition, shPosition[i], force, G, softFactor);
	}
	
	// the new force is returned to calculate_forces
	return newforce;
}

// Populates shared memory
__global__ void CalculateForces(glm::vec4 PosMass[], glm::vec4 Forces[], int blocks, float G, float softFactor)
{
	// A 'shared memory' buffer to store all the particle positions.
	// 'extern' denotes that the memory is dynamic
	extern __shared__ glm::vec4 shPosition[];

	// vector used to accumulate the forces
	glm::vec4 force = glm::vec4(0.0f, 0.0f, 0.0f, 1.0f);
	
	// get unique thread id
	int idxt = (blockIdx.x * blockDim.x) + threadIdx.x;
	
	// The particle position relative to the thread id
	// is stored.
	glm::vec4 myPosition = PosMass[idxt];

	// This for loop is only required if we are launching more blocks than threads
	for (int i = 0, tile = 0; i < blocks; i += blockDim.x, tile++)
	{
		int idx = tile *  blockDim.x + threadIdx.x;

		// Each thread contributes populating the shared memory buffer.
		shPosition[threadIdx.x] = PosMass[idx];

		force = TileCalculation(myPosition, force, G, softFactor);
		__syncthreads(); // artificial barrier
	}

	// Save force in global memory before the integration step.
	glm::vec4 force4 = glm::vec4(force.x, force.y, force.z, 1.0f);
	Forces[idxt] = force4;
}