/*
........................................................
Name:		Particle.h.
Details:	Header file containing the declaration of the particle struct and its members.
........................................................
Module:		Concurrent and Parallel Systems.
Author:		Noe Pages Puertas.
Date:		01/12/2014 | Napier University | 40127794@live.napier.ac.uk
*/

#pragma once

#include <graphics_framework.h>
#include <glm\glm.hpp>
#include <vector>

using namespace std;
using namespace graphics_framework;
using namespace glm;

struct Particle
{
	// To calculate the position of each particle, it is needed to store a set of variables.
	// 4 vectors with coordinates x,y,z.
	vec4		posMass;
	vec4		velocity;
	vec4		acceleration;
	vec4		eForces;

	// The struct members are filled with random numbers through the constructor.
	Particle();
	Particle(vec4 PosMass, vec4 Velocity, vec4 Acceleration);
	~Particle();
	
	// Receives the forces sent by the n-body calculations
	void AddForces(vec4 & addForce);
	// Euler integration
	void Update(float dt);
	// Prints position, velocity and acceleration of the particle;
	void MyDetails();
};