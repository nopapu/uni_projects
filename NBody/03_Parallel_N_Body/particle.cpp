/*
........................................................
Name:		particle.cpp.
Details:	cpp file containing constructor and the body of the particle member methods.
........................................................
Module:		Concurrent and parallel systems.
Author:		Noe Pages Puertas.
Date:		01/12/2014 | Napier University | 40127794@live.napier.ac.uk
*/

#pragma once

#include "particle.h"
#include <iostream>

using namespace std;

// Constructors
Particle::Particle(){};
Particle::Particle(vec4 PosMass, vec4 Velocity, vec4 Acceleration)
	: posMass(PosMass), velocity(Velocity), acceleration(Acceleration), eForces(vec4(0, 0, 0, 1)){}

// Destructor
Particle::~Particle(){};



// Vector addition keeps modifying the e_force member.
void Particle::AddForces(vec4 & addForce)
{
	// e_force will be used to calculate the acceleration in the update method.
	eForces += addForce;
}

// Update
void Particle::Update(float dt)
{
	// We work with
	eForces.w = 1.0f;
	
	// Euler integration
	
	// Newton's second law.
	acceleration = eForces / eForces.w;
	acceleration.w = 1.0f;
	
	// velocity is updated.
	velocity = velocity + (acceleration * dt);
	velocity.w = 1.0f;

	// the position is calculated.
	posMass = posMass + (velocity * dt);
	posMass.w = 1.0f;

	// eForce is set to 0 for the next integration
	eForces = vec4(0, 0, 0, 1.0f);
}

// Details about the particle
void Particle::MyDetails()
{
	cout << "_______________" << endl;
	cout << "My position is: " << posMass.x << ", " << posMass.y << ", " << posMass.z << endl;
	cout << "My velocity is: " << velocity.x << ", " << velocity.y << ", " << velocity.z << endl;
	cout << "My acceleration is: " << acceleration.x << ", " << acceleration.y << ", " << acceleration.z << endl;
}