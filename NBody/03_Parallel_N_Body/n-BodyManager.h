/*c
........................................................
Name:		Particle.h.
Details:	Header file containing the particle struct.
			It has been tweaked to work with buffers
........................................................
Module:		Concurrent and parallel systems.
Author:		Noe Pages Puertas.
Date:		01/12/2014 | Napier University | 40127794@live.napier.ac.uk
*/

# pragma once

#include <glm\glm.hpp>
#include <vector>
#include "particle.h"
#include <graphics_framework.h>

using namespace std;
using namespace graphics_framework;
using namespace glm;
using namespace std::chrono;

struct NBody
{
	// Buffers experiment
	vec4 ** posMass;
	vec4 ** eForces;
	
	int size;

	float G;				// Value for the constant gravity.
	float softFactor;		// stores the softening factor squared. 

	// Constructor and update.
	NBody::NBody();
	NBody::NBody(vec4 ** PossMass, float Gravity, float SoftFactor, int Size, vec4 ** EForces);

	
	void NBody::Initialise();
	void NBody::Update_N_Body();
};