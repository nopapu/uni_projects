/*c
........................................................
Name:		n-BodyManager.cpp.
Details:	cpp file containing the new integration method used to work with buffers in the CPU.
			It also has two constructors.
........................................................
Module:		Concurrent and Parallel Systems.
Author:		Noe Pages Puertas.
Date:		01/12/2014 | Napier University | 40127794@live.napier.ac.uk
*/

#include "n-BodyManager.h"

NBody::NBody(){};

NBody::NBody(vec4 ** PossMass, float Gravity, float SoftFactor, int Size, vec4 ** EForces)
	: posMass(PossMass), eForces(EForces), size(Size), G(Gravity), softFactor(SoftFactor)
{
	/*posMass = PossMass;
	eForces = EForces;

	size = Size;

	G = Gravity;
	softFactor = SoftFactor;*/
}

void NBody::Initialise(){};

void NBody::Update_N_Body()
{
	// a particle a is picked
	for (int i = 0; i < size; i++)
	{
		*eForces[i] = vec4(0,0,0,0);
		
		// and affected by all the other particles through the algorithm below
		for (int k = 0; k < size; k++)
		{
			//The if statement that checks if a particle influences itself is no longer needed.

			// This vector is the one going from vector a to vector b.
			vec4 vec2part = vec4(posMass[k]->x - posMass[i]->x, posMass[k]->y - posMass[i]->y, posMass[k]->z - posMass[i]->z, 1.0f);

			// Formula with softening factor.
			vec4 force = ((G * (*posMass[i]).w * (*posMass[k]).w) * vec2part) /
				glm::pow(glm::length(vec2part) * glm::length(vec2part) + softFactor, 0.6666666666666667f);

			force.w = 1.0f;

			// Finally, we add the resulting force.
			*eForces[i] += force;
		}
	}
}