/*c
........................................................
Name:		n-BodyManagerPar.cuh
Details:	This heather file contains the kernel declarations separated from the struct.
			kernels have to be separate from the struct.
........................................................
Module:		Concurrent and parallel systems.
Author:		Noe Pages Puertas.
Date:		01/12/2014 | Napier University | 40127794@live.napier.ac.uk
*/

#define CUDA_VERSION 6050
# pragma once

#include <cuda_runtime.h>
#include <device_launch_parameters.h>

#include <glm\glm.hpp>
#include <vector>
#include "particle.h"
#include <graphics_framework.h>

using namespace std;
using namespace graphics_framework;
using namespace glm;
using namespace std::chrono;

/*
	-> pointer to positions. In data.
	-> pointer to forces. Out data.

	* Initialise - sets up the memory in the GPU
	* Update - Reads from in buffer, process data, writes in out buffer.
*/

struct NBodyPar
{
	// vector of Particle pointers.
	glm::vec4 *buffPos;
	glm::vec4 *buffForce;
	
	int data_size;
	int elements;

	vector<glm::vec4> results;

	float G;				// A value for the constant gravity.
	float softFactor;		// When integrated in the update, creates a nice smoth acceleration and deceleration effect.
	
	// Constructor and update.
	NBodyPar::NBodyPar();
	NBodyPar::NBodyPar(int Data_Size, float Gravity, float SoftFactor, int ELEMENTS);
	
	void NBodyPar::Initialise();

	vector<glm::vec4> NBodyPar::Update_N_Body(vector<glm::vec4> CPUbuffin, vector<glm::vec4> CPUbuffout);
};

/*
	CUDA kernels
	N-body.cuh
*/

// This kernel is called by TileCalculation
__device__ glm::vec4 PartPartAttraction(glm::vec4 pi, glm::vec4 pj, glm::vec4 force, float G, float softFactor);

// This kernel is called by CalculateForces
__device__ glm::vec4 TileCalculation(glm::vec4 myPosition, glm::vec4 force, float G, float softFactor);

// This kernel is called by the N-body class Update method
__global__ void CalculateForces(glm::vec4 PosMass[], glm::vec4 Forces[], int blocks, float G, float softFactor);

