/*c
........................................................
Name:		main02.cpp.
Details:	The parallel, n-body simulation can be tested in this program.
			Some commented out code has been left as belongs to a experiment done
			with buffers for which there are files for.
........................................................
Module:		Concurrent and parallel systems.
Author:		Noe Pages Puertas.
Date:		01/11/2014 | Napier University | 40127794@live.napier.ac.uk |
*/

#define CUDA_VERSION 6050

#include "cuda_runtime.h"
#include "device_launch_parameters.h"

#include <stdio.h>
#include <graphics_framework.h>
#include <glm\glm.hpp>
#include <vector>
#include <iostream>
#include <chrono>
#include <random>
#include <fstream>
#include "particle.h"
#include "n-BodyManagerPar.cuh"
#include "n-BodyManager.h"

using namespace std;
using namespace graphics_framework;
using namespace glm;
using namespace std::chrono;

map<string, mesh> meshes;
effect eff;
texture tex;
target_camera cam;

# pragma region

const int NBODYSIZE = 4000;			// The number of bodies in the N_Body
const float G = 0.1f;				// A value for the constant gravity.
const float SOFTFACTOR = 16.0f;		// When integrated in the formula, creates a nice smoth acceleration and deceleration effect.

# pragma endregion constants

#pragma region files
ofstream tNBody("NBody.csv", ofstream::out);
ofstream tPart("Part.csv", ofstream::out);
#pragma endregion

// update iterations for the experiment
int iterations = 100;

vector <Particle*> Particles;
std::vector<graphics_framework::mesh> spheres;

NBodyPar myNBodyPar;

// Buffers to stream data from/to the GPU
vector<glm::vec4> in_CPUBuff;
vector<glm::vec4> out_CPUBuff;

bool load_content()
{
	// Create a random engine
	auto millis = duration_cast<milliseconds>(system_clock::now().time_since_epoch());
	default_random_engine e(millis.count());
	// Create a distribution - floats between a and b (a,b)
	uniform_real_distribution<float> posD(-75.0f, 75.0f);
	uniform_real_distribution<float> velD(-50.0f, 50.0f);

	// Spheres will represent the particles
	graphics_framework::geometry sphere_geom = graphics_framework::geometry_builder::create_sphere(20, 20);
	for (int i = 0; i < NBODYSIZE; ++i)
		spheres.push_back(graphics_framework::mesh(sphere_geom));

	for (unsigned int i = 0; i < NBODYSIZE; ++i)
	{
		// Create the particles
		Particles.push_back(new Particle(vec4(posD(e), posD(e), posD(e), 1.0f), vec4(velD(e), velD(e), velD(e), 1.0f), vec4(0, 0, 0, 1.0f)));
		
		//posBuff[i] = &Particles[i]->posMass;
		//eforBuff[i] = &Particles[i]->eForces;
		
		in_CPUBuff.push_back(Particles[i]->posMass);
		out_CPUBuff.push_back(Particles[i]->eForces);
	}

	// size of the memory needed in the GPU
	auto data_size = sizeof(glm::vec4)* NBODYSIZE;
	
	// Constructor of the NBody, GPU parallel struct
	NBodyPar x(data_size, G, SOFTFACTOR, NBODYSIZE);
	myNBodyPar = x;
	myNBodyPar.Initialise();
	
	/*
	
	Setting up 3D Scene.
	-> Not relevant for the experiment.

	*/

#pragma region 
	// Load texture
	tex = texture("..\\resources\\textures\\particle.gif");

	// Load in shaders
	eff.add_shader("..\\resources\\shaders\\simple_texture.vert", GL_VERTEX_SHADER);
	eff.add_shader("..\\resources\\shaders\\simple_texture.frag", GL_FRAGMENT_SHADER);
	// Build effect
	eff.build();

	// Set camera properties
	cam.set_position(vec3(500.0f, 0.0f, 50.0f));
	cam.set_target(vec3(0.0f, 0.0f, 0.0f));
	auto aspect = static_cast<float>(renderer::get_screen_width()) / static_cast<float>(renderer::get_screen_height());
	cam.set_projection(quarter_pi<float>(), aspect, 2.414f, 1000.0f);
#pragma endregion 

	return true;
}

bool update(float delta_time)
{	
	// Get positions and force accumulators
	if (iterations > 0)
	{
		#pragma omp parallel for /*schedule(runtime)*/ num_threads(4)
		for (int i = 0; i < NBODYSIZE; i++)
		{
			in_CPUBuff[i] = Particles[i]->posMass;
		}

		vector<glm::vec4> GPUback(NBODYSIZE);

		auto snbodyU = high_resolution_clock::now();

		// This for loop has been utilised to get 100 samples of times each update.
		// for (int i = 0; i < 100; i++)
		GPUback = myNBodyPar.Update_N_Body(in_CPUBuff, out_CPUBuff);

		auto enbodyU = high_resolution_clock::now();
		tNBody << "N-body update: ";
		long long ef = duration_cast<microseconds>(enbodyU - snbodyU).count() /*/ (float)100*/;
		tNBody << ", " << ef << endl;

		// Applying the force accumulator.
		#pragma omp parallel for /*schedule(runtime)*/ num_threads(4)
		for (int i = 0; i < NBODYSIZE; i++)
		{
			Particles[i]->eForces = GPUback[i];
		}

		auto spartU = high_resolution_clock::now();
		
		// Integrates particles.
		#pragma omp parallel for /*schedule(runtime)*/ num_threads(4)
		for (int i = 0; i < NBODYSIZE; i++)
		{
			Particles[i]->Update(delta_time);
			vec3 pos = vec3(Particles[i]->posMass.x, Particles[i]->posMass.y, Particles[i]->posMass.z);
			spheres[i].get_transform().position = pos;
		}

		auto epartU = system_clock::now();
		tPart << "Particle update: ";
		long long es = duration_cast<milliseconds>(enbodyU - snbodyU).count();
		tPart << ", " <<es << endl;

		// Update the camera
		cam.update(delta_time);
	}
	else if (iterations == 0)
	{
		tNBody.close();
		tPart.close();
	}
	
	return true;
}

/*

Render and run methods.
-> Not relevant for the experiment.

*/

bool render()
{
	for (int i = 0; i < spheres.size(); i++)
	{
		renderer::bind(eff);

		auto M = spheres[i].get_transform().get_transform_matrix();
		auto V = cam.get_view();
		auto P = cam.get_projection();
		auto MVP = P * V * M;
		// Set MVP matrix uniform
		glUniformMatrix4fv(
			eff.get_uniform_location("MVP"), // Location of uniform
			1, // Number of values - 1 mat4
			GL_FALSE, // Transpose the matrix?
			value_ptr(MVP)); // Pointer to matrix data

		//Bind and set texture
		renderer::bind(tex, 0);
		glUniform1i(eff.get_uniform_location("tex"), 0);

		// Render mesh
		renderer::render(spheres[0]);
	}

	return true;
}

void main()
{
	// Create application
	app application;
	// Set load content, update and render methods
	application.set_load_content(load_content);
	application.set_update(update);
	application.set_render(render);

	// Run application
	application.run();
}