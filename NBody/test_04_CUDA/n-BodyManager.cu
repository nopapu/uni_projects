#include "n-BodyManager.cuh"

__device__ glm::vec3 bodyBodyInteraction(glm::vec4 bi, glm::vec4 bj, glm::vec3 ai)
{
	glm::vec3 r;
	printf("In bodyBodyInteraction, bj is: %f %f %f \n", bj.x, bj.y, bj.z);
	printf("In bodyBodyInteraction, bi is: %f %f %f \n", bi.x, bi.y, bi.z);

	// r_ij  [3 FLOPS]
	r.x = bj.x - bi.x;
	r.y = bj.y - bi.y;
	r.z = bj.z - bi.z;

	printf("In bodyBodyInteraction, r is: %f %f %f \n", r.x, r.y, r.z);

	// distSqr = dot(r_ij, r_ij) + EPS^2  [6 FLOPS]
	float distSqr = r.x * r.x + r.y * r.y + r.z * r.z + 16.0f;

	// invDistCube =1/distSqr^(3/2)  [4 FLOPS (2 mul, 1 sqrt, 1 inv)]
	float distSixth = distSqr * distSqr * distSqr;
	float invDistCube = 1.0f / sqrtf(distSixth);

	// s = m_j * invDistCube [1 FLOP]
	float s = bj.w * invDistCube;

	// a_i =  a_i + s * r_ij [6 FLOPS]
	ai.x += r.x * s;
	ai.y += r.y * s;
	ai.z += r.z * s;

	return ai;
}

__device__ glm::vec3 tile_calculation(glm::vec4 myPosition, glm::vec3 force)
{
	int i;
	extern __shared__ glm::vec4 shPosition[];
	//printf("In tile_calculation, my position is: %f %f %f \n", myPosition.x, myPosition.y, myPosition.z);
	printf("In tile_calculation, blockDim is: %i \n", blockDim.x);
	//printf("In tile_calculation, my shPosition is: %f %f %f \n", shPosition[0].x, shPosition[0].y, shPosition[0].z);
	for (i = 0; i < blockDim.x; i++)
	{
		force = bodyBodyInteraction(myPosition, shPosition[i], force);
		//printf("In tile_calculation, my shPosition is: %f %f %f \n", shPosition[i].x, shPosition[i].y, shPosition[i].z);
		printf("my force is: %f %f %f \n", force.x, force.y, force.z);
	}

	return force;
}

__global__ void calculate_forces(glm::vec4 globalX[], glm::vec4 globalA[], int N)
{
	// A shared memory buffer to store the body positions.
	extern __shared__ glm::vec4 shPosition[];

	glm::vec4 myPosition;
	int i, tile;

	glm::vec3 force = glm::vec3(0.0f, 0.0f, 0.0f);
	// Global thread ID (represent the unique body index in the simulation) 
	int gtid = blockIdx.x * blockDim.x + threadIdx.x;
	printf("I am thread: %i \n", gtid);
	// This is the position of the body we are computing the acceleration for.
	myPosition = globalX[gtid];
	printf("and my Position is: %f %f %f \n", globalX[gtid].x, globalX[gtid].y, globalX[gtid].z);

	for (i = 0, tile = 0; i < N; i += blockDim.x, tile++)
	{
		int idx = tile * blockDim.x + threadIdx.x;

		// Each thread in the block participates in populating the shared memory buffer.
		shPosition[threadIdx.x] = globalX[idx];
		//printf("my shPosition is: %f %f %f \n", shPosition[threadIdx.x].x, shPosition[threadIdx.x].y, shPosition[threadIdx.x].z);
		// Synchronize guarantees all thread in the block have updated the shared memory
		// buffer.
		//__syncthreads();

		force = tile_calculation(myPosition, force);

		// Synchronize again to make sure all threads have used the shared memory
		// buffer before we overwrite the values for the next tile.
		//__syncthreads();
	}

	// Save the total acceleration in global memory for the integration step.
	glm::vec4 force4 = glm::vec4(force.x, force.y, force.z, 1.0f);
	globalA[gtid] = force4;
}