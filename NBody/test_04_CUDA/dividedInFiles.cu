/*c
........................................................
Name:		dividedInFiles
Details:	This test has been carried out with the intention of knitting cu and cuh files
........................................................
Module:		Concurrent and parallel systems.
Author:		Noe Pages Puertas.
Date:		02/12/2014 | Napier University | 40127794@live.napier.ac.uk
*/

#pragma once

#include <cuda_runtime.h>
#include <device_launch_parameters.h>

#include <chrono>
#include <random>

#include "../glm/glm/glm.hpp"
#include "n-BodyManager.cuh"

using namespace std::chrono;
using namespace std;

const int ELEMENTS = 4;
const int N = ELEMENTS;

int main()
{

	// Create a random engine
	auto millis = duration_cast<milliseconds>(system_clock::now().time_since_epoch());
	default_random_engine e(millis.count());
	// Create a distribution - floats between 0.0 and 10.0
	uniform_real_distribution<float> posD(0.0, 100.0f);

	// Initialise CUDA - select device
	cudaSetDevice(0);

	// Create host memory
	auto data_size = sizeof(glm::vec4)* ELEMENTS;

	glm::vec4 Pos[ELEMENTS]; // input array
	glm::vec4 Res[ELEMENTS]; // output array

	// Initialise input data
	for (unsigned int i = 0; i < ELEMENTS; ++i)
	{

		Pos[i].x = posD(e);
		Pos[i].y = posD(e);
		Pos[i].z = posD(e);
		Pos[i].w = 1.0f;
		printf("In the main, my position is: %f %f %f \n", Pos[i].x, Pos[i].y, Pos[i].z);
	}

	// Declare buffers
	glm::vec4 *buffer_Pos;
	glm::vec4 *buffer_Res;

	// Initialise buffers
	cudaMalloc((void**)&buffer_Pos, data_size);
	cudaMalloc((void**)&buffer_Res, data_size);

	//cudaMemcpy(dest, src, size, direction);

	// Write host data to device
	cudaMemcpy(buffer_Pos, &Pos[0], data_size, cudaMemcpyHostToDevice);
	cudaMemcpy(buffer_Res, &Res[0], data_size, cudaMemcpyHostToDevice);

	// Run kernel with one thread for each element
	// First value is number of blocks, second is threads per block.
	calculate_forces << < 1, ELEMENTS, sizeof(glm::vec4)* ELEMENTS >> >(buffer_Pos, buffer_Res, ELEMENTS);

	// Wait for kernel to complete
	cudaDeviceSynchronize();

	// Read output buffer back to the host
	cudaMemcpy(&Res[0], buffer_Res, data_size, cudaMemcpyDeviceToHost);

	cout << "...RESULTS..." << endl;
	for (int i = 0; i < ELEMENTS; ++i)
		cout << Res[i].x << ", " << Res[i].y << ", " << Res[i].z << ", " << Res[i].w << endl;

	//Clean up resources
	cudaFree(buffer_Pos);
	cudaFree(buffer_Res);

	system("pause");
	return 0;
}