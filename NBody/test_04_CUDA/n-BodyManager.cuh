#pragma once

#include <cuda_runtime.h>
#include <device_launch_parameters.h>

#include <iostream>
#include <chrono>
#include <random>

#include "../glm/glm/glm.hpp"

__device__ glm::vec3 bodyBodyInteraction(glm::vec4 bi, glm::vec4 bj, glm::vec3 ai);

__device__ glm::vec3 tile_calculation(glm::vec4 myPosition, glm::vec3 force);

__global__ void calculate_forces(glm::vec4 globalX[], glm::vec4 globalA[], int N);