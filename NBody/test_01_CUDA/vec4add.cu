/*c
........................................................
Name:		vec4add.cu
Details:	Test in which 2 glm::vec4 are added and copied in another buffer.
........................................................
Module:		Concurrent and parallel systems.
Author:		Noe Pages Puertas.
Date:		30/11/2014 | Napier University.
*/

#include <cuda_runtime.h>
#include <device_launch_parameters.h>
#include <iostream>
#include <vector>

#include "../glm/glm/glm.hpp"

using namespace std;

const int ELEMENTS = 1024;

void cuda_info()
{
	// Get CUDA device
	int device;
	cudaGetDevice(&device);

	// Get CUDA device properties
	cudaDeviceProp properties;
	cudaGetDeviceProperties(&properties, device);

	// Display properties
	cout << "Name: " << properties.name << endl;
	cout << "CUDA Capability: " << properties.major << "." << properties.minor << endl;
	cout << "Cores: " << properties.multiProcessorCount << endl;
	cout << "Memory: " << properties.totalGlobalMem / (1024 * 1024) << "MB" << endl;
	cout << "Clock freq: " << properties.clockRate / 1000 << "MHz" << endl;
}

__global__ void vec4add(const glm::vec4 *A, const glm::vec4 *B, glm::vec4 *C)
{
	// Get block index
	unsigned int block_idx = blockIdx.x;
	// Get thread index
	unsigned int thread_idx = threadIdx.x;
	// Get the number of threads per block
	unsigned int block_dim = blockDim.x;
	// Get the thread's unique ID - (block_idx * block_dim) + thread_idx;
	unsigned int idx = (block_idx * block_dim) + thread_idx;
	// Add corresponding locations of A and B and store in C
	C[idx] = A[idx] + B[idx];

}

int main()
{
	// Initialise CUDA - select device
	cudaSetDevice(0);
	cuda_info();

	//create host memory
	auto data_size = sizeof(glm::vec4)* ELEMENTS;
	vector<glm::vec4> A(ELEMENTS); // input array
	vector<glm::vec4> B(ELEMENTS); // input array
	vector<glm::vec4> C(ELEMENTS); // output array
	

	// Initialise input data
	for (unsigned int i = 0; i < ELEMENTS; ++i)
	{
		A[i].x = (float)i + 1.0f;
		A[i].y = (float)i + 1.0f;
		A[i].z = (float)i + 1.0f;
		A[i].w =  1.0f;
		B[i] = A[i];
		C[i] = glm::vec4(0,0,0,0);
	}
		
	//cout << A[100].x << B[100].y << C[100].z << A[100].w << endl;

	// Declare buffers
	glm::vec4 *buffer_A, *buffer_B, *buffer_C;

	// Initialise buffers
	cudaMalloc((void**)&buffer_A, data_size);
	cudaMalloc((void**)&buffer_B, data_size);
	cudaMalloc((void**)&buffer_C, data_size);

	//cudaMemcpy(dest, src, size, direction);

	// Write host data to device
	cudaMemcpy(buffer_A, &A[0], data_size, cudaMemcpyHostToDevice);
	cudaMemcpy(buffer_B, &B[0], data_size, cudaMemcpyHostToDevice);

	// Run kernel with one thread for each element
	// First value is number of blocks, second is threads per block.
	// Max 1024 threads per block
	vec4add <<< ELEMENTS / 1024, 1024 >>>(buffer_A, buffer_B, buffer_C);

	// Wait for kernel to complete
	cudaDeviceSynchronize();

	// Read output buffer back to the host
	cudaMemcpy(&C[0], buffer_C, data_size, cudaMemcpyDeviceToHost);

	cout << C[200].x << ", "<< C[200].y << ", "<< C[200].z << ", "<< C[200].w << endl;

	//Clean up resources
	cudaFree(buffer_A);
	cudaFree(buffer_B);
	cudaFree(buffer_C);

	system("pause");
	return 0;
}

/*

// Initialise CUDA - select device
cudaSetDevice(0);
cuda_info();

//create host memory
auto data_size = sizeof(glm::vec4)* ELEMENTS;
vector<glm::vec4> A(ELEMENTS); // input array
vector<glm::vec4> B(ELEMENTS); // input array
vector<glm::vec4> C(ELEMENTS); // output array


// Initialise input data
for (unsigned int i = 0; i < ELEMENTS; ++i)
{
A[i].x = (float)i + 1.0f;
A[i].y = (float)i + 1.0f;
A[i].z = (float)i + 1.0f;
A[i].w =  1.0f;
B[i] = A[i];
C[i] = glm::vec4(0,0,0,0);
}

//cout << A[100].x << B[100].y << C[100].z << A[100].w << endl;

// Declare buffers
glm::vec4 *buffer_A, *buffer_B, *buffer_C;

// Initialise buffers
cudaMalloc((void**)&buffer_A, data_size);
cudaMalloc((void**)&buffer_B, data_size);
cudaMalloc((void**)&buffer_C, data_size);

//cudaMemcpy(dest, src, size, direction);

// Write host data to device
cudaMemcpy(buffer_A, &A[0], data_size, cudaMemcpyHostToDevice);
cudaMemcpy(buffer_B, &B[0], data_size, cudaMemcpyHostToDevice);

// Run kernel with one thread for each element
// First value is number of blocks, second is threads per block.
// Max 1024 threads per block
vec4add <<< ELEMENTS / 1024, 1024 >>>(buffer_A, buffer_B, buffer_C);

// Wait for kernel to complete
cudaDeviceSynchronize();

// Read output buffer back to the host
cudaMemcpy(&C[0], buffer_C, data_size, cudaMemcpyDeviceToHost);

cout << C[200].x << ", "<< C[200].y << ", "<< C[200].z << ", "<< C[200].w << endl;

//Clean up resources
cudaFree(buffer_A);
cudaFree(buffer_B);
cudaFree(buffer_C);

system("pause");
return 0;



*/