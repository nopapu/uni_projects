/*c
........................................................
Name:		main01.cpp.
Details:	The n-body simulation is tested in this program.
........................................................
Module:		Concurrent and parallel systems.
Author:		Noe Pages Puertas.
Date:		26/11/2014 | Napier University | 40127794@live.napier.ac.uk
*/

#pragma once

#include "particle.h"
#include "vectorMaths.h"

#include <vector>
#include <iostream>
#include <chrono>
#include <random>
#include <fstream>

using namespace std;
using namespace std::chrono;

# pragma region

const int _nBodySize	= 2;		// The number of bodies in the N_Body.
const int _dtIterations	= 1000;		// The number of iterations that the N_Body will be updated.
const float CONST_DT	= 0.001;	// A constant fraction of time to update the N_Body.
const float CONST_G		= 50.0f;	// A value for the constant gravity.

# pragma endregion constants

#define DBG_ASSERT(test) if ( !(test) ) { __debugbreak(); }
inline bool IsValid(double d)
{
	if (_isnan(d))
	{
		return true;
	}

	return false;
}
#define DBG_VALID(val) if (IsValid(val)) { DBG_ASSERT(false); }

// This method distributes the particles in the 3D space and gives them an initial velocity.
void Generate_N_Body(vector <Particle*>& N_Body)
{
	//Create a random engine
	auto millis = duration_cast<milliseconds>(system_clock::now().time_since_epoch());
	default_random_engine e(millis.count());
	//Create a distribution - floats between 0.0 and 10.0
	uniform_real_distribution<float> posD(0.0, 10.0f);
	uniform_real_distribution<float> velD(0.0, 1.0f);
	uniform_real_distribution<float> accD(0.0, 0.5f);

	for (unsigned int i = 0; i < _nBodySize; ++i)
	{
		N_Body.push_back(new Particle(
			Vector3(posD(e), posD(e), posD(e)), // position
			Vector3(0, 0, 0),					// velocity
			Vector3(0, 0, 0),					// acceleration
			1.0f));
	}
}


//This method computes the influence of the N_Body over a particle.
void Update_N_Body(vector <Particle*>& N_Body)
{
	// a particle a is picked
	for (int i = 0; i < N_Body.size(); i++)
	{
		// and affected by all the other particles through the algorithm below
		for (int k = 0; k < N_Body.size(); k++)
		{
			if (i != k)
			{
				// This vector is the one going from vector a to vector b.
				Vector3 vec2part = N_Body[k]->position - N_Body[i]->position;

				// Unit length vector representation of vec2part.
				Vector3 force = vec2part / vec2part.Length();

				// the projection is calculated.
				force = force * (CONST_G * ((N_Body[i]->mass * N_Body[k]->mass)
					/ vec2part.LengthSq()));

				// Finally, we add the resulting force.
				N_Body[i]->AddForces(force);
			}
		}
	}
}

void main()
{
	// Container where the particles are.
	vector <Particle*> N_Body;
	
	// Initialises an n number of particles.
	// These are distributed on the 3d space providing them acceleration and velocity.
	Generate_N_Body(N_Body);

	cout << "Nummer of particles in the n-body: " << _nBodySize << "." << endl;
	cout << "**************************" << endl;
	cout << endl;
	cout << "N-body before updating: " << endl;
	for (int i = 0; i < N_Body.size(); i++)
		N_Body[i]->MyDetails();
	
	// The next for loop statement executes the instructions to update the the N_Body.
	// It loops as many times as it is defined by the _dtIterations global variable.
	for (int frame = 0; frame < _dtIterations; ++frame)
	{
		Update_N_Body(N_Body);
		// This for loop updates the position of each particle at once;
		for (int i = 0; i < N_Body.size(); i++)
			N_Body[i]->Update(CONST_DT);
	}
	
	// Results
	cout << endl;
	cout << "**************************" << endl;
	cout << "Time step: " << CONST_DT << "." << endl;
	cout << "Constant G: " << CONST_G << "." << endl;
	cout << "**************************" << endl;
	cout << endl;
	cout << "N-body state after " << _dtIterations << " iterations:" << endl;
	for (int i = 0; i < N_Body.size(); i++)
		N_Body[i]->MyDetails();

	// Deleting pointers inside the container
	for (int i = 0; i < N_Body.size(); i++)
		delete N_Body[i] ;
	
	// Clearing container
	N_Body.clear();
	
	system("pause");
	return;
}