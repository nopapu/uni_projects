/*c
........................................................
Name:		threeKernels.cu
Details:	Test to implement the algorithm shown in CUDA gems.
			http://http.developer.nvidia.com/GPUGems3/gpugems3_ch31.html
			http://3dgep.com/cuda-case-study-n-body-simulation/
........................................................
Module:		Concurrent and parallel systems.
Author:		Noe Pages Puertas.
Date:		03/12/2014 | Napier University | 40127794@live.napier.ac.uk
*/

#include <cuda_runtime.h>
#include <device_launch_parameters.h>
#include <iostream>
#include <chrono>
#include <random>

#include "../glm/glm/glm.hpp"

using namespace std::chrono;
using namespace std;

const int ELEMENTS = 4;
const int N = ELEMENTS;

__device__ float EPS2 = 8.0f;

__device__ glm::vec3 bodyBodyInteraction(glm::vec4 bi, glm::vec4 bj, glm::vec3 ai)
{
	glm::vec3 r;
	printf("In bodyBodyInteraction, bj is: %f %f %f \n", bj.x, bj.y, bj.z);
	printf("In bodyBodyInteraction, bi is: %f %f %f \n", bi.x, bi.y, bi.z);

	r.x = bj.x - bi.x;
	r.y = bj.y - bi.y;
	r.z = bj.z - bi.z;
	
	printf("In bodyBodyInteraction, r is: %f %f %f \n", r.x, r.y, r.z);

	float distSqr = r.x * r.x + r.y * r.y + r.z * r.z + EPS2;

	float distSixth = distSqr * distSqr * distSqr;
	float invDistCube = 1.0f / sqrtf(distSixth);

	float s = bj.w * invDistCube;

	ai.x += r.x * s;
	ai.y += r.y * s;
	ai.z += r.z * s;

	return ai;
}

__device__ glm::vec3 tile_calculation(glm::vec4 myPosition, glm::vec3 force)
{
	int i;
	extern __shared__ glm::vec4 shPosition[];
	//printf("In tile_calculation, my position is: %f %f %f \n", myPosition.x, myPosition.y, myPosition.z);
	printf("In tile_calculation, blockDim is: %i \n", blockDim.x);
	//printf("In tile_calculation, my shPosition is: %f %f %f \n", shPosition[0].x, shPosition[0].y, shPosition[0].z);
	for (i = 0; i < blockDim.x; i++)
	{
		force = bodyBodyInteraction(myPosition, shPosition[i], force);
		//printf("In tile_calculation, my shPosition is: %f %f %f \n", shPosition[i].x, shPosition[i].y, shPosition[i].z);
		printf("my force is: %f %f %f \n", force.x, force.y, force.z);
	}

	return force;
}

__global__ void calculate_forces(glm::vec4 globalX[], glm::vec4 globalA[])
{
	// A shared memory buffer to store the body positions.
	extern __shared__ glm::vec4 shPosition[];

	glm::vec4 myPosition;
	int i, tile;

	glm::vec3 force = glm::vec3(0.0f, 0.0f, 0.0f);
	// Global thread ID (represent the unique body index in the simulation) 
	int gtid = blockIdx.x * blockDim.x + threadIdx.x;
	printf("I am thread: %i \n", gtid);
	// This is the position of the body we are computing the acceleration for.
	myPosition = globalX[gtid];
	printf("and my Position is: %f %f %f \n", globalX[gtid].x, globalX[gtid].y, globalX[gtid].z);

	for (i = 0, tile = 0; i < N; i += blockDim.x, tile++)
	{
		int idx = tile * blockDim.x + threadIdx.x;

		// Each thread in the block participates in populating the shared memory buffer.
		shPosition[threadIdx.x] = globalX[idx];
		//printf("my shPosition is: %f %f %f \n", shPosition[threadIdx.x].x, shPosition[threadIdx.x].y, shPosition[threadIdx.x].z);
		// Synchronize guarantees all thread in the block have updated the shared memory
		// buffer.
		//__syncthreads();

		force = tile_calculation(myPosition, force);

		// Synchronize again to make sure all threads have used the shared memory
		// buffer before we overwrite the values for the next tile.
		//__syncthreads();
	}

	// Save the total acceleration in global memory for the integration step.
	glm::vec4 force4 = glm::vec4(force.x, force.y, force.z, 1.0f);
	globalA[gtid] = force4;
}


int main()
{
	// Create a random engine
	auto millis = duration_cast<milliseconds>(system_clock::now().time_since_epoch());
	default_random_engine e(millis.count());
	// Create a distribution - floats between 0.0 and 10.0
	uniform_real_distribution<float> posD(0.0, 100.0f);

	// Initialise CUDA - select device
	cudaSetDevice(0);

	// Create host memory
	auto data_size = sizeof(glm::vec4)* ELEMENTS;

	glm::vec4 Pos[ELEMENTS]; // input array
	glm::vec4 Res[ELEMENTS]; // output array

	// Initialise input data
	for (unsigned int i = 0; i < ELEMENTS; ++i)
	{	
		Pos[i].x = posD(e);
		Pos[i].y = posD(e);
		Pos[i].z = posD(e);
		Pos[i].w = 1.0f;
		printf("In the main, my position is: %f %f %f \n", Pos[i].x, Pos[i].y, Pos[i].z);
	}
	
	// Declare buffers
	glm::vec4 *buffer_Pos;
	glm::vec4 *buffer_Res;

	// Initialise buffers
	cudaMalloc((void**)&buffer_Pos, data_size);
	cudaMalloc((void**)&buffer_Res, data_size);

	//cudaMemcpy(dest, src, size, direction);

	// Write host data to device
	cudaMemcpy(buffer_Pos, &Pos[0], data_size, cudaMemcpyHostToDevice);
	cudaMemcpy(buffer_Res, &Res[0], data_size, cudaMemcpyHostToDevice);

	// Run kernel with one thread for each element
	// First value is number of blocks, second is threads per block.
	// Max 1024 threads per block
	calculate_forces <<< 1, ELEMENTS, sizeof(glm::vec4)* ELEMENTS>>>(buffer_Pos, buffer_Res);

	// Wait for kernel to complete
	cudaDeviceSynchronize();

	// Read output buffer back to the host
	cudaMemcpy(&Res[0], buffer_Res, data_size, cudaMemcpyDeviceToHost);

	cout << "...RESULTS..." << endl;
	for (int i = 0; i < ELEMENTS; ++i)
		cout << Res[i].x << ", " << Res[i].y << ", " << Res[i].z << ", " << Res[i].w << endl;
	
	//Clean up resources
	cudaFree(buffer_Pos);
	cudaFree(buffer_Res);

	system("pause");
	return 0;
}