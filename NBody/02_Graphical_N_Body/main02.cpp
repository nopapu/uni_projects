/*c
........................................................
Name:		main02.cpp.
Details:	The Graphical, n-body simulation is tested in this program.
........................................................
Module:		Concurrent and parallel systems.
Author:		Noe Pages Puertas.
Date:		29/11/2014 | Napier University | 40127794@live.napier.ac.uk | 40127794@live.napier.ac.uk
*/

#include <graphics_framework.h>
#include <glm\glm.hpp>
#include <vector>
#include <iostream>
#include <chrono>
#include <random>
#include <fstream>
#include "particle.h"
#include "n-BodyManager.h"

using namespace std;
using namespace graphics_framework;
using namespace glm;
using namespace std::chrono;

map<string, mesh> meshes;
effect eff;
texture tex;
target_camera cam;

# pragma region

const int _nBodySize	= 1000;			// The number of bodies in the N_Body
const float CONST_G		= 0.005f;		// A value for the constant gravity.
float _softFactor		= 16.0f;		// When integrated in the formula, creates a nice smoth acceleration and deceleration effect.

# pragma endregion constants

#pragma region files
ofstream tNBody("NBody.csv", ofstream::out);
ofstream tPart("Part.csv", ofstream::out);
#pragma endregion

int iterations = 101;

vector <Particle*> N_Body;
std::vector<graphics_framework::mesh> spheres;
NBody myNBody;

//This method computes the influence of the N_Body over a particle.
void Update_N_Body(vector <Particle*>& N_Body)
{
	// a particle a is picked
	for (int i = 0; i < N_Body.size(); i++)
	{
		// and affected by all the other particles through the algorithm below
		for (int k = 0; k < N_Body.size(); k++)
		{
			if (i != k)
			{
				// This vector is the one going from vector a to vector b.
				vec3 vec2part = N_Body[k]->position - N_Body[i]->position; // specified as r in the report.

				// The vector defining the distance from the two vectors is stored as a unit length vector.
				vec3 force = vec2part / glm::length(vec2part);

				// the projection is calculated. 2 possible integrations:
				
				// 1st.- Normal formula.
				//force = ((CONST_G * N_Body[i]->mass * N_Body[k]->mass) / glm::length(vec2part) *  glm::length(vec2part)) * force;

				// 2nd.- Formula with softening factor.
				force = ((CONST_G * N_Body[i]->mass * N_Body[k]->mass) * vec2part) /
					glm::pow(glm::length(vec2part) * glm::length(vec2part) + _softFactor, 0.6666666666666667f);

				// Finally, we add the resulting force.
				N_Body[i]->AddForces(force);
			}
		}
	}
}

bool load_content()
{
	//Create a random engine
	auto millis = duration_cast<milliseconds>(system_clock::now().time_since_epoch());
	default_random_engine e(millis.count());
	//Create a distribution - floats between 0.0 and 10.0
	uniform_real_distribution<float> posD(-20.0f, 20.0f);
	uniform_real_distribution<float> velD(-20.0f, 20.0f);

	graphics_framework::geometry sphere_geom = graphics_framework::geometry_builder::create_sphere(20,20);
	
	for (int i = 0; i < _nBodySize; ++i)
		spheres.push_back(graphics_framework::mesh(sphere_geom));
	
	for (unsigned int i = 0; i < _nBodySize; ++i)
	{
		N_Body.push_back(new Particle(
			vec3(posD(e), posD(e), posD(e)),	// position
			vec3(velD(e), velD(e), velD(e)),	// velocity vec3(velD(e), velD(e), velD(e)) vec3(0, 0, 0)
			vec3(0, 0, 0),						// acceleration
			1.0f));
	}
	
	NBody x(N_Body, 0.05f, 16.0f);
	myNBody = x;

	
	// Load texture
	tex = texture("..\\resources\\textures\\particle.gif");
	
	// Load in shaders
	eff.add_shader("..\\resources\\shaders\\simple_texture.vert", GL_VERTEX_SHADER);
	eff.add_shader("..\\resources\\shaders\\simple_texture.frag", GL_FRAGMENT_SHADER);
	// Build effect
	eff.build();

	// Set camera properties
	cam.set_position(vec3(50.0f, 0.0f, 50.0f));
	cam.set_target(vec3(0.0f, 0.0f, 0.0f));
	auto aspect = static_cast<float>(renderer::get_screen_width()) / static_cast<float>(renderer::get_screen_height());
	cam.set_projection(quarter_pi<float>(), aspect, 2.414f, 1000.0f);
	return true;
}

bool update(float delta_time)
{
	// cout << thread::hardware_concurrency() << endl;
	//iterations--;
	// Update n-body
	// Update_N_Body(N_Body);
	if (iterations > 0)
	{
		auto snbodyU = system_clock::now();

		//for (int i = 0; i < 100; i++)
		myNBody.Update_N_Body();

		auto enbodyU = system_clock::now();
		tNBody << "N-body update: ";
		float ef = (float)duration_cast<milliseconds>(enbodyU - snbodyU).count() /*/ 40.0f*/;
		tNBody << ", " << ef << endl;

		auto spartU = system_clock::now();
		
		// Update particles
		for (int i = 0; i < _nBodySize; i++)
		{
			N_Body[i]->Update(delta_time);
			spheres[i].get_transform().position = N_Body[i]->position;
		}

		auto epartU = system_clock::now();
		tPart << "Particle update: ";
		tPart << ", " << duration_cast<milliseconds>(epartU - spartU).count() << endl;

		// Update the camera
		cam.update(delta_time);
	}
	else if (iterations == 0)
	{
		tNBody.close();
		tPart.close();
	}
	return true;
}

bool render()
{
	for (int i = 0; i < spheres.size(); i++)
	{
		renderer::bind(eff);
		
		auto M = spheres[i].get_transform().get_transform_matrix();
		auto V = cam.get_view();
		auto P = cam.get_projection();
		auto MVP = P * V * M;
		// Set MVP matrix uniform
		glUniformMatrix4fv(
			eff.get_uniform_location("MVP"), // Location of uniform
			1, // Number of values - 1 mat4
			GL_FALSE, // Transpose the matrix?
			value_ptr(MVP)); // Pointer to matrix data

		// Bind and set texture
		renderer::bind(tex, 0);
		glUniform1i(eff.get_uniform_location("tex"), 0);

		// Render mesh
		renderer::render(spheres[0]);
	}

	return true;
}

void main()
{
	// Create application
	app application;
	// Set load content, update and render methods
	application.set_load_content(load_content);
	application.set_update(update);
	application.set_render(render);
	
	// Run application
	application.run();
}