/*c
........................................................
Name:		n-BodyManager.h.
Details:	Header file containing the n-body struct.
			Contains data and functions to update the position of a particle in a time step.
			Five member attributes, a constructor and three member methods.
........................................................
Module:		Concurrent and parallel systems.
Author:		Noe Pages Puertas.
Date:		29/11/2014 | Napier University | 40127794@live.napier.ac.uk
*/

# pragma once

#include <glm\glm.hpp>
#include <vector>
#include "particle.h"
#include <graphics_framework.h>
#include <thread>

using namespace std;
using namespace graphics_framework;
using namespace glm;
using namespace std::chrono;

struct NBody
{
	// vector of Particle pointers.
	vector <Particle*> n_body;
	
	float G;			// A value for the constant gravity.
	float SFACTOR;		// When integrated in the update, creates a nice smoth acceleration and deceleration effect.

	// 2 Constructors, the destructor and update.
	NBody::NBody();
	NBody::NBody(vector <Particle*> & N_Body, const float GRAVITY, const float SFACTOR);
	NBody::~NBody();
	void NBody::Update_N_Body();
};