/*c
........................................................
Name:		particle.h.
Details:	Header file containing the particle struct.
			Contains data and functions to update the position of a particle in a time step.
			Five member attributes, a constructor and three member methods.
........................................................
Module:		Concurrent and parallel systems.
Author:		Noe Pages Puertas.
Date:		29/11/2014 | Napier University | 40127794@live.napier.ac.uk
*/

#pragma once

#include <graphics_framework.h>
#include <glm\glm.hpp>
#include <vector>

using namespace std;
using namespace graphics_framework;
using namespace glm;

struct Particle
{
	// To calculate the position of each particle, it is needed to store a set of variables.
	// 4 vectors with coordinates x,y,z.
	vec3		position;
	vec3		velocity;
	vec3		e_forces;
	vec3		acceleration;
	
	// 2 scalars to operate with the vectors.
	float		mass;

	// The constructor is intended to be filled with random numbers.
	Particle::Particle(vec3 Position, vec3 Velocity, vec3 acceleration, float mass);
	// Method that prints the position, velocity and acceleration details of each particle;
	void Particle::MyDetails();
	// And two methods from which the position will finally be updated.
	void Particle::AddForces(vec3 & addForce);
	void Particle::Update(float dt);
};