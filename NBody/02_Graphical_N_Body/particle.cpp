/*c
........................................................
Name:		particle.cpp.
Details:	cpp file containing constructor and the definition of the particle member methods.
........................................................
Module:		Concurrent and parallel systems.
Author:		Noe Pages Puertas.
Date:		26/11/2014 | Napier University.
*/

#pragma once

#include "particle.h"
#include <iostream>

using namespace std;

// Constructor
Particle::Particle(vec3 Position, vec3 Velocity, vec3 Acceleration, float Mass) : position(Position), velocity(Velocity), acceleration(Acceleration), mass(Mass)
{	
	e_forces = vec3(0, 0, 0);
	//MyDetails();
}

// Details about the particle
void Particle::MyDetails()
{
	cout << "_______________" << endl;
	cout << "My position is: " << position.x << ", " << position.y << ", " << position.z << endl;
	cout << "My velocity is: " << velocity.x << ", " << velocity.y << ", " << velocity.z << endl;
	cout << "My acceleration is: " << acceleration.x << ", " << acceleration.y << ", " << acceleration.z << endl;
}

// Vector addition keeps modifying the e_force member.
void Particle::AddForces(vec3 & addForce)
{
	// e_force will be used to calculate the acceleration in the update method.
	e_forces += addForce;
}

// Update
void Particle::Update(float dt)
{
	// Newton's second law.
	acceleration = e_forces / mass;
	
	// velocity is updated.
	velocity = velocity + (acceleration * dt);

	// the position is calculated.
	position = position + (velocity * dt);

	// the forces are set to 0 for the next integration
	e_forces = vec3(0, 0, 0);
}