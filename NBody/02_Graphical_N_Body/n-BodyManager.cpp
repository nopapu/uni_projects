/*c
........................................................
Name:		n-BodyManager.cpp.
Details:	cpp file containing the new integration method used until the end of the project.
........................................................
Module:		Concurrent and parallel systems.
Author:		Noe Pages Puertas.
Date:		29/11/2014 | Napier University | 40127794@live.napier.ac.uk
*/

#include "n-BodyManager.h"

NBody::NBody():G(4),SFACTOR(4){};
NBody::NBody(vector <Particle*> & N_Body, float GRAVITY, float SOFTFACTOR) : n_body(N_Body), G(GRAVITY), SFACTOR(SOFTFACTOR) {}
NBody::~NBody(){};

void NBody::Update_N_Body()
{

#pragma omp parallel for /*schedule(runtime)*/ num_threads(4) // thread::hardware_concurrency()
	// a particle a is picked
	for (int i = 0; i < n_body.size(); i++)
	{
		// and influenced by all the other particles through the algorithm below
		for (int k = 0; k < n_body.size(); k++)
		{
			//The if statement that checks if a particle influences itself is no longer needed.
			
			// This vector is the one going from particle i to vector k.
			vec3 vec2part = n_body[k]->position - n_body[i]->position; // specified as r in the report.

			// Formula with softening factor.
			vec3 force = ((G * n_body[i]->mass * n_body[k]->mass) * vec2part) /
				glm::pow(glm::length(vec2part) * glm::length(vec2part) + SFACTOR, 0.6666666666666667f);

			// Finally, we add the resulting force to the picked particle.
			n_body[i]->AddForces(force);
		}
	}
}