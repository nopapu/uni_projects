#include <iostream>
#include <chrono>
#include <iomanip> 

using namespace std;
using namespace std::chrono;

/*

Operations with SIMD
- It has been useful when making tests before implementation in the project 01_ParallelSolution.

*/

#pragma region
/*
SSE intrinsics
http://msdn.microsoft.com/en-us/library/c8c5hx3b(v=vs.71).aspx
http://msdn.microsoft.com/en-us/library/c8c5hx3b(v=vs.71).aspx#vcref_mm_cvtps_pd
http://msdn.microsoft.com/en-us/library/3h8aa97w(v=vs.71).aspx
SSE2 intrinsics
http://www.univ-orleans.fr/lifo/Members/Sylvain.Jubertie/doc/SIMD/html/group___s_s_e2.html
*/
#pragma endregion LinksOfInterest

// Defines the data size to work on
const unsigned int SIZE = 1000;
const unsigned int sSIZE = 100;

// Used to validate the result.  This is related to the data size
const double CHECK_VALUE = 12.0;

void testm128d(__m128d ** a, double value)
{
	int elements = 0;
	for (int i = 0; i < SIZE; ++i)
	{
		for (int k = 0; k < SIZE/2; ++k)
		{
			a[i][k].m128d_f64[0] = value + double(i) + double (k) ;
			a[i][k].m128d_f64[1] = (value + double(i) + double(k)) * 2.0;
			elements += 2;
		}
	}
	cout << "elements in the structure: "<< elements << endl;
}


int main()
{
	const double *pi = new const double (3.1415926535897);
	const double *two = new const double (2.0);

	/*
	:::: OPERATIONS TEST ::::
	*/

	// Initialising values in the __m128d
	__m128d a;
	a.m128d_f64[0] = 0.1111111;
	a.m128d_f64[1] = 9.8888;

	cout << "I am a[0]: "<<a.m128d_f64[0] << ", I am a[1]: " << a.m128d_f64[1] << endl;

	// Loading values in the __m128d
	a = _mm_loadl_pd(a, two);
	a =	_mm_loadh_pd(a, pi);
	
	cout << "I am a[0]: " << a.m128d_f64[0] << ", I am a[1]: " << a.m128d_f64[1] << endl;

	// Another way to load
	__m128d b;
	b.m128d_f64[0] = 0.1111111;
	b.m128d_f64[1] = 5.0;

	// c is initialised to 0;
	__m128d c;
	c.m128d_f64[0] = 0;
	c.m128d_f64[1] = 0;

	// function multiply pd multiplies the two members
	c = _mm_mul_pd(a, b);
	cout << "I am c[0]: " << c.m128d_f64[0] << ", I am c[1]: " << c.m128d_f64[1] << endl;

	// function multiply sd multiplies just the first member
	c = _mm_mul_sd(c, a);
	cout << "I am c[0]: " << c.m128d_f64[0] << ", I am c[1]: " << c.m128d_f64[1] << endl;

	//initialising a 1000 x 1000 elements data structure
	//first element will be 1000 and the second 500
	double value = 0.9;
	__m128d **data = new __m128d*[SIZE];
	for (int i = 0; i < SIZE; ++i)
		data[i] = new __m128d[SIZE / 2];
	
	testm128d(data, value);
	cout << "I am the last value in data : " << data[999][499].m128d_f64[1] << endl;

	/*
	:: END ::
	*/

	/*
	:::: ARRAY TEST ::::
	*/
	
	// normal array 'original' and _m128d declared
	double** original = (double**)_aligned_malloc(sSIZE * sizeof(double*), 16);
	for (int i = 0; i < sSIZE; ++i)
		original[i] = (double*)_aligned_malloc(sSIZE * sizeof(double), 16);

	//double ** original = new double*[sSIZE];

	//for (int i = 0; i < sSIZE; ++i)
		//original[i] = new double[sSIZE];

	__m128d **SIMDv = new __m128d*[sSIZE];
	for (int i = 0; i < sSIZE; ++i)
		SIMDv[i] = new __m128d[sSIZE / 2];
	
	
	// original is filled.
	for (int i = 0; i < sSIZE; ++i)
		for (int j = 0; j < sSIZE; ++j)
			original[i][j] = ((double)i * (double) j)/3.0;

	cout << "\nvalue [50][50] in original array : " << original[50][50] << "and [99][99] : " << original[99][99] << endl;

	// casting 'original' to 'SIMD'
	
	SIMDv = (__m128d**)original;
	cout << "cast to a __m128d" << endl;
	cout << "Value contained in [50][25].m128d_f64[0]: " << SIMDv[50][25].m128d_f64[0] << "and [99][49].m128d_f64[1]: " << SIMDv[99][49].m128d_f64[1] << endl;
	
	/*
	:: END ::
	*/
	
	/*
	:: EXPERIMENT WITH FUNCTIONS REQUIRED FOR DAXPY ::
	*/
	
	double * oricola = &original[50][0];
	__m128d* columna = (__m128d*)oricola;
	//columna = &SIMDv[50][0];

	double * oricolb = &original[51][0];
	__m128d* columnb = (__m128d*)oricolb;
	//columnb = &SIMDv[51][0];

	cout << "columna before SIMD : " << columna[49].m128d_f64[1] << endl;
	cout << "columnb : " << columnb[49].m128d_f64[1] << endl;

	//setting the two doubles to be 2.0
	__m128d bytwo = _mm_set1_pd(2.0);
	cout << "I am by two: " << bytwo.m128d_f64[0] << " , " << bytwo.m128d_f64[1] << endl;

	// every row of columna will equal itself by columnb multiplied by 2.0


	for (int i = 0; i < 50; i++)
	{
		//__m128d x = _mm_mul_pd(columnb[i], bytwo);
		columna[i] = _mm_add_pd(columna[i], _mm_mul_pd(columnb[i], bytwo));
	}
	
	cout << "columna new: " << columna[49].m128d_f64[1] << endl;
	cout << "same as the correspondent value in original: " << original[50][99] << endl;
	
	/*
	:: END ::
	*/
	
	/*
	:: EXPERIMENT WITH FUNCTIONS REQUIRED FOR IDAMAX ::
	*/
	__m128d abstest;

	abstest.m128d_f64[0] = -10.89;
	abstest.m128d_f64[1] = -0.87;

	__m128d absolluting = _mm_max_pd(_mm_sub_pd(_mm_setzero_pd(), abstest), abstest);

	cout << "absolluting.m128d_f64[0]: " << absolluting.m128d_f64[0] << endl;
	cout << "absolluting.m128d_f64[1]: " << absolluting.m128d_f64[1] << endl;
	/*
	:: END ::
	*/


	for (int i = 0; i < sSIZE; ++i)
		_aligned_free(original[i]);
	_aligned_free(original);

	system("pause");
	return 0;
}