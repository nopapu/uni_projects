Module -> Concurrent and Parallel Systems

--------------------------------------------------------------
LINPACK.dox explains the project and 
				displays the results obtained
--------------------------------------------------------------

Student -> STU40127794, Noe Pages Puertas


Double click in the CourseWork_Part1 Visual Studio Icon.



::::FINAL VERSION:


*04_TimedCleanParallelSolution: Linpack1000 parallelised using SIMD instructions and multithreading



Recommended Solution Configurations:

Select 'Release' in the solution configurations.
Click in the Debug menu and select 'start without debugging'



:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
::Other projects inside Coursework_Part1::


*00_OriginalSequentialSolution:	the given sequential solution

*01_ParallelSolution:		the project where more work has been done.

*02_SIMDTestingGround:		is the project where the operations with SIMD have been tested.

*03_InitialiseRACE:		is a project created at the beginning to test the speed of the matgen method in parallel and sequential.

*05_thousandArrayAndThreads:	is a small project created to compare the sequential vs parallel in small arrays and simple operations



