#include <iostream>
#include <omp.h>
#include <chrono>
#include <thread>
#include <vector>
#include <random>
#include <math.h>

using namespace std;
using namespace std::chrono;

/*

Early test to test the speed of matgen in parallel.
- It was abandoned due to the complexity of matgen at the beginning. Dgefa became a priority.

*/

/* Links of interest
http://msdn.microsoft.com/en-us/library/c8c5hx3b(v=vs.71).aspx
http://msdn.microsoft.com/en-us/library/c8c5hx3b(v=vs.71).aspx#vcref_mm_cvtps_pd
http://stackoverflow.com/questions/4430839/sse-convert-m128-and-m128i-into-two-m128d
*/

// Defines the data size to work on
const unsigned int SIZE = 1000;
// Used to validate the result.  This is related to the data size
const double CHECK_VALUE = 12.0;


//SEQUENTIAL FUNCTIONS
double matgen(double **a, int lda, int n, double *b)
{
	double norma = 0.0;
	int init = 1325;

	for (int i = 0; i < n; ++i)
	{
		for (int j = 0; j < n; ++j)
		{
			init = 3125 * init % 65536;
			a[j][i] = (static_cast<double>(init)-32768.0) / 16384.0;
			norma = (a[j][i] > norma) ? a[j][i] : norma;
		}
	}

	for (int i = 0; i < n; ++i)
		b[i] = 0.0;

	for (int j = 0; j < n; ++j)
	{
		for (int i = 0; i < n; ++i)
			b[i] += a[j][i];
	}
	
	return norma;
}

void initialise(double **a, double *b, double &ops, double &norma, double lda)
{
	auto start = system_clock::now();

	long long nl = static_cast<long long>(SIZE);
	ops = (2.0 * static_cast<double>((nl * nl * nl))) / 3.0 + 2.0 * static_cast<double>((nl * nl));
	
	norma = matgen(a, lda, SIZE, b); //1.99982

	auto end = system_clock::now();
	auto total = end - start;

	cout << "::<- initialise time: " << duration_cast<milliseconds>(total).count() << " mllscnds" << endl;
	cout << "ops value: " << ops << endl;
	cout << "norma value: " << norma << endl;
}

int modCalc( int init, int val)
{
	//bool found = false;
	//int value = 0;
	//int it = 0;
	
	/*while (!found)
	{
		int result = 65536 * it;
		if (result > div)
		{
			value = div - ((it - 1) * 65536);
			found = true;
		}
		it++;
	}*/
	
	/*int value = div / 65536;

	value = div - (65536 * value);*/

	//step1
	val = init *= 3125; //very big number

	//step2
	init *= 0.0000152587890625; //small number


	init = val-(init * 65536);

	return init;
}



//PARALLEL FUNCTIONS
double matgen_PAR(double **a, int lda, int n, double *b)
{
	double norma = 0.0;
	int init = 1325;
	int val = 0;

	for (int i = 0; i < n; ++i)
	{
		for (int j = 0; j < n; ++j)
		{
			init = modCalc(init,val);
			a[j][i] = (static_cast<double>(init)-32768.0) * 0.00006103515625;
			norma = (a[j][i] > norma) ? a[j][i] : norma;
		}
	}

	for (int i = 0; i < n; ++i)
		b[i] = 0.0;
	for (int j = 0; j < n; ++j)
	{
		for (int i = 0; i < n; ++i)
			b[j] += a[i][j];
	}

	return norma;
}

void initialise_PAR(double **a, double *b, double &ops, double &norma, double lda)
{
	auto start = system_clock::now();

	long long nl = static_cast<long long>(SIZE);
	ops = (2.0 * static_cast<double>((nl * nl * nl))) / 3.0 + 2.0 * static_cast<double>((nl * nl));
	
	norma = matgen_PAR(a, lda, SIZE, b); //1.99982

	auto end = system_clock::now();
	auto total = end - start;

	cout << "::<- initialise_PAR time: " << duration_cast<milliseconds>(total).count() << " mllscnds" << endl;
	cout << "ops value: " << ops << endl;
	cout << "norma value: " << norma << endl;
}
//::MAIN::
int main(int argc, char **argv)
{
	unsigned int random;
	double PARval;
	double val;

	for (int i = 0; i < 5; ++i)
	{
		// sequential variables
		double **a = new double*[SIZE];
		for (int i = 0; i < SIZE; ++i)
			a[i] = new double[SIZE];
		double *b = new double[SIZE];

		double ldaa = static_cast<double>(SIZE);
		double lda = ldaa + 1;
		double ops, norma;
		
		//end

		// parallel variables
		double **ap = new double*[SIZE];
		for (int i = 0; i < SIZE; ++i)
			ap[i] = new double[SIZE];
		double *bp = new double[SIZE];

		double ldaap = static_cast<double>(SIZE);
		double ldap = ldaap + 1;
		double opsp, normap;
		//end
		
		// RACE //
		
		initialise(a, b, ops, norma, lda);
		initialise_PAR(ap, bp, opsp, normap, ldap);
		//initialise_PAR(ap, bp, opsp, normap, ldap);
		
		// end //

		// random number generator
		auto millis = duration_cast<milliseconds>(system_clock::now().time_since_epoch());
		default_random_engine e(millis.count());
		uniform_int_distribution<unsigned int> distribution(0, SIZE);

		random = distribution(e);

		PARval = bp[random];
		val = b[random];
		
		if (val == PARval && norma == normap)
			cout << "CORRECT results : )" << endl;
		else
			cout << "INcorrect results : (" << endl;
		
		// Free the memory
		for (int i = 0; i < SIZE; ++i)
			delete[] a[i];
		delete[] a;
		delete[] b;

		for (int i = 0; i < SIZE; ++i)
			delete[] ap[i];
		delete[] ap;
		delete[] bp;
		//end
	}

	system("pause");
	return 0;
}