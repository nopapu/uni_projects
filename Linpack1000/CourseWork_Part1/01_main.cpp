#include <iostream>
#include <omp.h>
#include <chrono>
#include <thread>
#include <vector>
#include <fstream>
#include <cassert>
#include <malloc.h>

using namespace std;
using namespace std::chrono;

/*

Testing version.
- The methods in here are all clean and tidy in project 04_TimedCleanParallelSolution

Important!
1-	Run in Release:
2-	Click debug, select start without debugging.

*/


#define DBG_ASSERT(test) if ( !(test) ) { __debugbreak(); }
inline bool IsValid(double d)
{
	if (_isnan(d))
	{
		return true;
	}

	return false;
}
#define DBG_VALID(val) if (IsValid(val)) { DBG_ASSERT(false);}

using namespace std;
using namespace std::chrono;

/* Links of interest
 http://msdn.microsoft.com/en-us/library/c8c5hx3b(v=vs.71).aspx
 http://msdn.microsoft.com/en-us/library/c8c5hx3b(v=vs.71).aspx#vcref_mm_cvtps_pd
 http://stackoverflow.com/questions/4430839/sse-convert-m128-and-m128i-into-two-m128d
 */

// Defines the data size to work on
const unsigned int SIZE = 1000;
// Used to validate the result.  This is related to the data size`
const double CHECK_VALUE = 12.0;
//Create a new file
//ofstream data("data.csv", ofstream::out);
//ofstream cent("centSIMD.csv", ofstream::out);
ofstream timepar("timeParallel.csv", ofstream::out);

double normagen(double **a, int n, double *b)
{
	double norma = 0.0;
	int init = 1325;

	auto num_threads = 8;//thread::hardware_concurrency();
	int share = n / num_threads;

	for (int i = 0; i < n; ++i)
	{
		for (int j = 0; j < n; ++j)
		{
			init = 3125 * init % 65536;
			a[j][i] = (static_cast<double>(init)-32768.0) * 0.00006103515625;
			norma = (a[j][i] > norma) ? a[j][i] : norma;
		}
	}

	for (int j = 0; j < n; ++j)
	{
		b[j] = 0.0;
		for (int i = 0; i < n; ++i)
			b[j] += a[i][j];
	}

	return norma;
}

void matgen(double **a, int n, double *b)
{
	int init = 1325;

	auto num_threads = 8;//thread::hardware_concurrency();
	int share = n / num_threads;
	
	for (int i = 0; i < n; ++i)
	{
		for (int j = 0; j < n; ++j)
		{
			init = 3125 * init % 65536;
			a[j][i] = (static_cast<double>(init)-32768.0) * 0.00006103515625;
		}
	}

	for (int j = 0; j < n; ++j)
	{
		b[j] = 0.0;
		for (int i = 0; i < n; ++i)
			b[j] += a[i][j];
	}
}

int idamax(double *col_k, int k) // n is - 1 at each iteration of the for loop it comes from // l = idamax(n - k, col_k, k) + k;
{
	double dmax, dtemp;
	int itemp = k;

	dmax = abs(col_k[k]);

	for (int i = k; i < SIZE ; ++i)
	{
		dtemp = abs(col_k[i]);
		if (dtemp > dmax) //Finds the biggest
		{
			itemp = i;
			dmax = dtemp;
		}
	}

	return itemp;
}

// Scales a vector by a constant
void dscal( double t, double *col_k, int kp1) //dscal( t, col_k, kp1)
{
	for (int i = kp1; i < SIZE ; ++i)
		col_k[i] *= t;
}

// Scales a vector by a constant
void dscalSIMD(double t, double *col_k, int kp1) //dscal( t, col_k, kp1)
{
	__m128d* dataa = (__m128d*)col_k;
	int start = (int)(kp1 / 2);
	int start_rem = kp1 % 2;
	__m128d temp_simd = _mm_set1_pd(t);

	if (start_rem == 1)
	{
		dataa[start].m128d_f64[1] *= t;
	}
	
	for (int i = start_rem + start; i < SIZE / 2; ++i)
		dataa[i] =  _mm_mul_pd(dataa[i], temp_simd);
}

// Constant times a vector plus a vector
void daxpydgesl(int dn, double temp, double *col_k, int kp1, double *dy) //dgefa: daxpy(n - kp1, t, col_k, kp1, col_j); dgesl: daxpy(n - kp1, t, &a[k][0], kp1, b); dgesl: daxpy(k, t, &a[k][0], 0, b);
{
	for (int i = 0; i < dn; ++i)
		dy[i + kp1] += temp * col_k[i + kp1];
}

// Constant times a vector plus a vector
void daxpy(double temp, double *col_k, int kp1, double *col_j) //dgesl: daxpy(k, t, &a[k][0], 0, b);
{
	for (int i = kp1; i < SIZE; ++i)
		col_j[i] += temp * col_k[i];
}

void daxpySIMD(const double temp, double *col_k, const int kp1, double *dy) //dgefa: daxpy(n - kp1, t, col_k, kp1, col_j); dgesl: daxpy(n - kp1, t, &a[k][0], kp1, b); dgesl: daxpy(k, t, &a[k][0], 0, b);
{

	__m128d* dataa = (__m128d*)dy;
	__m128d* datab = (__m128d*)col_k;
	__m128d temp_simd = _mm_set1_pd(temp);
	//DBG_ASSERT(dn > 0 && dn < 1000);
	//DBG_ASSERT(kp1 > 0);
	
	int start = (int)(kp1 / 2);
	int start_rem = kp1 % 2;

	int iter = SIZE / 2;

	// original code
	/*for (int i = 0; i < dn; ++i)
		dy[i + kp1] += temp * col_k[i + kp1];*/

	if (kp1 == 5)
		int x = 0;


	if (start_rem == 1)
	{
		dataa[start].m128d_f64[1] += datab[start].m128d_f64[1] * temp;
		
		//DBG_VALID(dataa[start].m128d_f64[1]);
	}
	
	for (int i = start_rem + start; i < iter; ++i)
	{
		//DBG_ASSERT(i <= 499);
		//DBG_ASSERT(i + start <= 499);
		
		 dataa[i] = _mm_add_pd(dataa[i], _mm_mul_pd(datab[i], temp_simd));
		
		//DBG_VALID(dataa[i + start].m128d_f64[0]);
		//DBG_VALID(dataa[i + start].m128d_f64[1]);
	}

}

void daxpySIMD2(int nd, const double temp, double *col_k, const int kp1, double *dy) //dgesl: daxpy(k, t, &a[k][0], 0, b); k is 9 to 0 and kp1 always 0
{

	__m128d* dataa = (__m128d*)dy;
	__m128d* datab = (__m128d*)col_k;
	__m128d temp_simd = _mm_set1_pd(temp);

	int start = (int)(kp1 / 2);
	int start_rem = kp1 % 2;

	int iter = nd / 2;
	int iter_rem = nd % 2;

	if (kp1 == 0)
		int x = 0;


	for (int i = start; i < iter; ++i)
	{
		dataa[i] = _mm_add_pd(dataa[i], _mm_mul_pd(datab[i], temp_simd));
	}

	if (iter_rem == 1)
	{
		dataa[iter].m128d_f64[0] += datab[iter].m128d_f64[0] * temp;
	}

}

// Performs Gaussian elimination with partial pivoting
int dgefa(double **a, int n, int *ipvt) //dgefa(a, lda, n, ipvt);
{
	// Pointers to columns being worked on
	double *col_k, *col_j;

	double temp;
	int kp1;
	int nm1 = n - 1;
	int info = 0;

	for (int k = 0; k < nm1; ++k) // For loop 998 times
	{
		// Set pointer for col_k to relevant column in a
		col_k = &a[k][0]; // 
		kp1 = k + 1; //it starts at 1 and goes up to 999

		// Find pivot index
		ipvt[k] = idamax(col_k, k); // ipvt is filled with the value that l returns that is the maximum absolute value from n minus k to the end

		// Zero pivot means that this column is already triangularized
		if (col_k[ipvt[k]] != 0)
		{
			// Check if we need to interchange
			temp = col_k[ipvt[k]];
			if (ipvt[k] != k)
			{
				col_k[ipvt[k]] = col_k[k];
				col_k[k] = temp;
			}

			// Compute multipliers
			temp = -1.0 / temp;
			dscalSIMD(temp, col_k, kp1); // n-kp1 starts at 999, t is kept, col_k again and kp1.
		
			// Row elimination with column indexing
			#pragma omp parallel for num_threads(4) private (temp)
			for (int j = kp1; j < n; ++j) // for loop from current value of kp1 to 1000
			{
				// Set pointer for col_j to relevant column in a
				col_j = &a[j][0]; // Is parallisable

				temp = col_j[ipvt[k]]; // Is parallisable
				if (ipvt[k] != k) // Is parallisable
				{
					col_j[ipvt[k]] = col_j[k];
					col_j[k] = temp;
				}
				daxpySIMD(temp, col_k, kp1, col_j);
			}
		}	
	}
	
	

	ipvt[n - 1] = n - 1;
	if (a[n - 1][n - 1] == 0)
		info = n - 1;

	return info;
}

// Solves the system a * x = b using the factors computed in dgeco or dgefa
void dgesl(double **a, int n, int *ipvt, double *b)
{
	double t;
	int k, l, nm1, kp1;

	nm1 = n - 1;

	// Solve a * x = b.  First solve l * y = b
	for (k = 0; k < nm1; ++k)
	{
		l = ipvt[k];
		t = b[l];
		if (l != k)
		{
			b[l] = b[k];
			b[k] = t;
		}
		kp1 = k + 1;
		daxpySIMD(t, &a[k][0], kp1, b); // daxpy(n - kp1, t, &a[k][0], kp1, b);
	}

	// Now solve u * x = y
	for (int i = n-1 ; i > -1; --i) //for (int i = 0; i < n; ++i)
	{
		//k = n - (i + 1);
		b[i] /= a[i][i];
		t = -b[i];
		daxpySIMD2(i, t, &a[i][0], 0, b); // daxpySIMD2(k, t, &a[k][0], 0, b);
	}
}

// Multiply matrix m times vector x and add the result to vector y
void dmxpy(int n, double *b, double *x, double **a) //dmxpy(n, b, x, a);
{
	for (int j = 0; j < n; ++j)
	for (int i = 0; i < n; ++i)
		b[i] += x[j] * a[j][i];
}

// Estimates roundoff in quantities of size x
double epslon(double x)
{
	double eps = 0.0;
	double a = 4.0 / 3.0; //4.0/3.0
	double b, c;

	while (eps == 0)
	{
		b = a - 1.0;
		c = b * 3;
		eps = abs(c - 1.0);
	}

	return eps * abs(x);
}

// Initialises the system
void initialise(double **a, double *b, double &ops, double &norma, double lda)
{
	long long nl = static_cast<long long>(SIZE);
	ops = (2.0 * static_cast<double>((nl * nl * nl))) / 3.0 + 2.0 * static_cast<double>((nl * nl));
	matgen(a, SIZE, b);
}

// Runs the benchmark
void run(double **a, double *b, int &info, double lda, int n, int *ipvt)
{
	info = dgefa(a, n, ipvt);
	/*for (int i = 0; i < SIZE; i++)
	{
		cent << endl;
		for (int j = 0; j < SIZE; j++)
			cent << "," << a[i][j];
	}
	cent.close();*/
	dgesl(a, n, ipvt, b);
}

// Validates the result
void validate(double **a, double *b, double *x, double &norma, double &normx, double &resid, double lda, int n)
{
	double eps, residn;
	double ref[] = { 6.0, 12.0, 20.0 };

	for (int i = 0; i < n; ++i)
		x[i] = b[i];

	norma = normagen(a, n, b);

	for (int i = 0; i < n; ++i)
		b[i] = -b[i];

	dmxpy(n, b, x, a);
	resid = 0.0;
	normx = 0.0;
	for (int i = 0; i < n; ++i)
	{
		resid = (resid > abs(b[i])) ? resid : abs(b[i]);
		normx = (normx > abs(x[i])) ? normx : abs(x[i]);
	}

	eps = epslon(1.0);
	residn = resid / (n * norma * normx * eps);
	if (residn > CHECK_VALUE)
	{
		cout << "Validation failed!" << endl;
		cout << "Computed Norm Res = " << residn << endl;
		cout << "Reference Norm Res = " << CHECK_VALUE << endl;
	}
	else
	{
		cout << "Calculations are correct!" << endl;
		cout << "Computed Norm Res = " << residn << endl;
		cout << "Reference Norm Res = " << CHECK_VALUE << endl;
	}
}

int main(int argc, char **argv)
{
	for (int k = 0; k < 100; k++){
		double** a = new double*[SIZE];/*(double**)_aligned_malloc(SIZE * sizeof(double*), 16);*/
		for (int i = 0; i < SIZE; ++i)
			a[i] = (double*)_aligned_malloc(SIZE * sizeof(double), 16);
			//a[i] = new double[SIZE];

		//double *b = new double[SIZE];
		double *b = (double*)_aligned_malloc(SIZE * sizeof(double), 16);

		double *x = new double[SIZE];
		int *ipvt = new int[SIZE];

		double ldaa = static_cast<double>(SIZE);
		double lda = ldaa + 1;
		double ops, norma, normx;
		double resid;
		int info;

		auto start = system_clock::now();
		timepar << "Timing attempt: " << k;

		initialise(a, b, ops, norma, lda);
		run(a, b, info, lda, SIZE, ipvt);
		validate(a, b, x, norma, normx, resid, lda, SIZE);

		auto end = system_clock::now();
		auto total = end - start;
		timepar << ", " << duration_cast<milliseconds>(total).count() << endl;

		
		for (int i = 0; i < SIZE; ++i)
			_aligned_free(a[i]);
		delete[] a;
		//delete[] b;
		_aligned_free(b);
		delete[] x;
		delete[] ipvt;
	}
	timepar.close();
	system("pause");
	return 0;
}