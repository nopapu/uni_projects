#include <iostream>
#include <chrono>
#include <fstream>

using namespace std;
using namespace std::chrono;

/*

Final version.
1- Dscal and 3 different methods of Daxpy use SIMD.
2- One mp statement in dgefa.

The Speed up against the sequential version is over 3.

Important!
1-	Run in Release: 
2-	Click debug, select start without debugging.

*/

/* Links of interest
http://msdn.microsoft.com/en-us/library/c8c5hx3b(v=vs.71).aspx
http://msdn.microsoft.com/en-us/library/c8c5hx3b(v=vs.71).aspx#vcref_mm_cvtps_pd
http://stackoverflow.com/questions/4430839/sse-convert-m128-and-m128i-into-two-m128d
*/

// It has to stay 1000
const unsigned int SIZE = 1000;
// Used to validate the result.  This is related to the data size
const double CHECK_VALUE = 12.0;

//files
#pragma region files
ofstream tMain("main.csv", ofstream::out);
ofstream tInitialise("initialise.csv", ofstream::out);
ofstream tRun("run.csv", ofstream::out);
ofstream tValidate("validate.csv", ofstream::out);
ofstream tDgefa("tDgefa.csv", ofstream::out);
ofstream tDaxpy("tDaxpy.csv", ofstream::out);
ofstream tDgesl("tDgesl.csv", ofstream::out);
#pragma endregion

//This method fills up matrixes a and b
double matgen(double **a, int n, double *b)
{
	double norma = 0.0;
	int init = 1325;

	for (int i = 0; i < n; ++i)
	{
		for (int j = 0; j < n; ++j)
		{
			init = 3125 * init % 65536;
			a[j][i] = (static_cast<double>(init)-32768.0) / 16384.0;
			norma = (a[j][i] > norma) ? a[j][i] : norma;
		}
	}
	for (int i = 0; i < n; ++i)
		b[i] = 0.0;
	for (int j = 0; j < n; ++j)
	{
		for (int i = 0; i < n; ++i)
			b[i] += a[j][i];
	}

	return norma;
}


#pragma region clean sequential functions used by dgesl and dgefa

//Finds the maximum absolute number from a given starting point until the end of the column
int idamax(double *col_k, int k) // n is - 1 at each iteration of the for loop it comes from
{
	double dmax, dtemp;
	int itemp = k;

	dmax = abs(col_k[k]);

	for (int i = k; i < SIZE; ++i)
	{
		dtemp = abs(col_k[i]);
		if (dtemp > dmax)
		{
			itemp = i;
			dmax = dtemp;
		}
	}

	return itemp;
}

// Scales a vector by a constant
void dscal(double t, double *col_k, int kp1)
{
	for (int i = kp1; i < SIZE; ++i)
		col_k[i] *= t;
}

// Constant times a vector plus a vector
void daxpy(double temp, double *col_k, int kp1, double *col_j)
{
	for (int i = kp1; i < SIZE; ++i)
		col_j[i] += temp * col_k[i];
}

// Constant times a vector plus a vector
void daxpydgesl(int dn, double temp, double *col_k, int kp1, double *dy)
{
	for (int i = 0; i < dn; ++i)
		dy[i + kp1] += temp * col_k[i + kp1];
}

#pragma endregion

#pragma region dscal SIMD version
// Scales a vector by a constant starting from kp1 until the end of the column
void dscalSIMD(double t, double *col_k, int kp1) //dscal( t, col_k, kp1)
{
	__m128d* dataa = (__m128d*)col_k;
	int start = (int)(kp1 / 2);
	int start_rem = kp1 % 2;
	__m128d temp_simd = _mm_set1_pd(t);

	if (start_rem == 1)
	{
		dataa[start].m128d_f64[1] *= t;
	}

	for (int i = start_rem + start; i < SIZE / 2; ++i)
		dataa[i] = _mm_mul_pd(dataa[i], temp_simd);
}
#pragma endregion

#pragma region daxpy SIMD methods
//This version of SIMD256 should be ready for implementation.
void daxpySIMD256(const double temp, double *col_k, const int kp1, double *dy)
{
	__m256d* dataa = (__m256d*)dy;
	__m256d* datab = (__m256d*)col_k;
	__m256d temp_simd = _mm256_set1_pd(temp);

	int start = (int)(kp1 / 4);
	int start_rem = kp1 % 4;

	int iter = SIZE / 4;

	int isreminder = 0;
	//If there is remainder, the position in the for loop has to be moved 1 position
	if (start_rem > 0)
		isreminder = 1;

	//There will be 3 possible remainders when dividing the starting point by 4.
	switch (start_rem)
	{
	case 1:
		dataa[start].m256d_f64[3] += datab[start].m256d_f64[3] * temp;
		break;
	case 2:
		dataa[start].m256d_f64[2] += datab[start].m256d_f64[2] * temp;
		dataa[start].m256d_f64[3] += datab[start].m256d_f64[3] * temp;
		break;
	case 3:
		dataa[start].m256d_f64[1] += datab[start].m256d_f64[1] * temp;
		dataa[start].m256d_f64[2] += datab[start].m256d_f64[2] * temp;
		dataa[start].m256d_f64[3] += datab[start].m256d_f64[3] * temp;
		break;
	}

	//SIMD
	for (int i = isreminder + start; i < iter; ++i)
	{
		dataa[i] = _mm256_add_pd(dataa[i], _mm256_mul_pd(datab[i], temp_simd));
	}

}

// This version of SIMD works for the two methods in the dgefa and one in dgesl
void daxpySIMD(const double temp, double *col_k, const int kp1, double *dy)
{
	__m128d* dataa = (__m128d*)dy;
	__m128d* datab = (__m128d*)col_k;
	__m128d temp_simd = _mm_set1_pd(temp);

	int start = (int)(kp1 / 2);
	int start_rem = kp1 % 2;

	int iter = SIZE / 2;

	// case for the odd indexes
	if (start_rem == 1)
	{
		dataa[start].m128d_f64[1] += datab[start].m128d_f64[1] * temp;
	}
	
	// note the increment of the starting position if there is remainder
	for (int i = start_rem + start; i < iter; ++i)
	{
		dataa[i] = _mm_add_pd(dataa[i], _mm_mul_pd(datab[i], temp_simd));
	}
}

// second for loop of dgesl daxpy
// In a 10 by 10 matrix, k ranges from 9 to 0 and kp1 always 0
// when k = 9, it loops until position 8
// when k = 0, it does not do anything.
void daxpySIMD2(int nd, const double temp, double *col_k, const int kp1, double *dy) 
{

	__m128d* dataa = (__m128d*)dy;
	__m128d* datab = (__m128d*)col_k;
	__m128d temp_simd = _mm_set1_pd(temp);

	int start = (int)(kp1 / 2);
	int start_rem = kp1 % 2;

	int iter = nd / 2;
	int iter_rem = nd % 2;

	for (int i = start; i < iter; ++i)
	{
		dataa[i] = _mm_add_pd(dataa[i], _mm_mul_pd(datab[i], temp_simd));
	}

	if (iter_rem == 1)
	{
		dataa[iter].m128d_f64[0] += datab[iter].m128d_f64[0] * temp;
	}
}

#pragma endregion

// Find here the omp statement that sends the 4 threads
#pragma region run methods
// Performs Gaussian elimination with partial pivoting
int dgefa(double **a, int n, int *ipvt)
{
	// Pointers to columns being worked on
	double *col_k, *col_j;

	double temp;
	int kp1;
	int nm1 = n - 1;
	int info = 0;

	for (int k = 0; k < nm1; ++k) // For loop 998 times
	{
		// Set pointer for col_k to relevant column in a
		col_k = &a[k][0];
		kp1 = k + 1;

		// Find pivot index
		ipvt[k] = idamax(col_k, k);

		// Zero pivot means that this column is already triangularized
		if (col_k[ipvt[k]] != 0)
		{
			// Check if we need to interchange
			temp = col_k[ipvt[k]];
			if (ipvt[k] != k)
			{
				col_k[ipvt[k]] = col_k[k];
				col_k[k] = temp;
			}

			// Compute multipliers
			temp = -1.0 / temp;
			dscalSIMD(temp, col_k, kp1);

			// Row elimination with column indexing
			#pragma omp parallel for num_threads(4) private (temp)
			for (int j = kp1; j < n; ++j)
			{
				col_j = &a[j][0];

				temp = col_j[ipvt[k]];
				if (ipvt[k] != k)
				{
					col_j[ipvt[k]] = col_j[k];
					col_j[k] = temp;
				}

				daxpySIMD(temp, col_k, kp1, col_j);
			}
		}
	}

	ipvt[n - 1] = n - 1;
	if (a[n - 1][n - 1] == 0)
		info = n - 1;

	return info;
}

// Solves the system a * x = b using the factors computed in dgeco or dgefa
void dgesl(double **a, int n, int *ipvt, double *b)
{
	double t;
	int k, l, nm1, kp1;

	nm1 = n - 1;

	// Solve a * x = b.  First solve l * y = b
	for (k = 0; k < nm1; ++k)
	{
		l = ipvt[k];
		t = b[l];
		if (l != k)
		{
			b[l] = b[k];
			b[k] = t;
		}
		kp1 = k + 1;
		daxpySIMD(t, &a[k][0], kp1, b); // daxpy(n - kp1, t, &a[k][0], kp1, b);
	}

	// Now solve u * x = y
	for (int i = n - 1; i > -1; --i) //for (int i = 0; i < n; ++i)
	{
		//k = n - (i + 1);
		b[i] /= a[i][i];
		t = -b[i];
		daxpySIMD2(i, t, &a[i][0], 0, b); // daxpySIMD2(k, t, &a[k][0], 0, b);
	}
}
#pragma endregion

// no work has been done in these methods.
#pragma region validate methods
// Multiply matrix m times vector x and add the result to vector y
void dmxpy(int n, double *b, double *x, double **a) //dmxpy(n, b, x, a);
{
	for (int j = 0; j < n; ++j)
	for (int i = 0; i < n; ++i)
		b[i] += x[j] * a[j][i];
}

// Estimates roundoff in quantities of size x
double epslon(double x)
{
	double eps = 0.0;
	double a = 4.0 / 3.0; //4.0/3.0
	double b, c;

	while (eps == 0)
	{
		b = a - 1.0;
		c = b * 3;
		eps = abs(c - 1.0);
	}

	return eps * abs(x);
}

#pragma endregion

// They are cleaner and contain time measuring statements.
#pragma region initialise run and validate
// Initialises the system
void initialise(double **a, double *b, double &ops, double &norma, double lda)
{
	auto start = system_clock::now();

	long long nl = static_cast<long long>(SIZE);
	ops = (2.0 * static_cast<double>((nl * nl * nl))) / 3.0 + 2.0 * static_cast<double>((nl * nl));
	matgen(a, SIZE, b);

	auto end = system_clock::now();
	auto total = end - start;

	tInitialise << "initialise attempt: ";
	tInitialise << ", " << duration_cast<milliseconds>(total).count() << endl;
}

// Runs the benchmark
void run(double **a, double *b, int &info, double lda, int n, int *ipvt)
{
	auto start = system_clock::now();

	auto sdgefa = system_clock::now();
	
	info = dgefa(a, n, ipvt);
	
	auto edgefa = system_clock::now();
	auto tdgefa = edgefa - sdgefa;
	tDgefa << "dgfa attempt: ";
	tDgefa << ", " << duration_cast<milliseconds>(tdgefa).count() << endl;

	auto sgesl = system_clock::now();
	
	dgesl(a, n, ipvt, b);
	
	auto egesl = system_clock::now();
	auto tgesl = egesl - sgesl;
	tDgesl << "dgesl attempt: ";
	tDgesl << ", " << duration_cast<milliseconds>(tgesl).count() << endl;

	auto end = system_clock::now();
	auto total = end - start;

	tRun << "run attempt: ";
	tRun << ", " << duration_cast<milliseconds>(total).count() << endl;
}

// Validates the result
void validate(double **a, double *b, double *x, double &norma, double &normx, double &resid, double lda, int n)
{
	auto start = system_clock::now();

	double eps, residn;
	double ref[] = { 6.0, 12.0, 20.0 };

	for (int i = 0; i < n; ++i)
		x[i] = b[i];

	norma = matgen(a, n, b);

	for (int i = 0; i < n; ++i)
		b[i] = -b[i];

	dmxpy(n, b, x, a);
	resid = 0.0;
	normx = 0.0;
	for (int i = 0; i < n; ++i)
	{
		resid = (resid > abs(b[i])) ? resid : abs(b[i]);
		normx = (normx > abs(x[i])) ? normx : abs(x[i]);
	}

	eps = epslon(1.0);
	residn = resid / (n * norma * normx * eps);
	if (residn > CHECK_VALUE)
	{
		cout << "Validation failed!" << endl;
		cout << "Computed Norm Res = " << residn << endl;
		cout << "Reference Norm Res = " << CHECK_VALUE << endl;
	}
	else
	{
		cout << "Calculations are correct!" << endl;
		cout << "Computed Norm Res = " << residn << endl;
		cout << "Reference Norm Res = " << CHECK_VALUE << endl;
	}
	auto end = system_clock::now();
	auto total = end - start;

	tValidate << "validate attempt: ";
	tValidate << ", " << duration_cast<milliseconds>(total).count() << endl;
}
#pragma endregion


int main(int argc, char **argv)
{
	for (int k = 0; k < 100; ++k)
	{
		// Allocate data on the heap
		double** a = new double*[SIZE];
		// for the work with SIMD, a and b use the C style version of 'new'
		// the statement says: create a big block of memory on the heap capable to hold SIZE doubles.
		// this big block will be divided in 16 bytes chunks
		for (int i = 0; i < SIZE; ++i)
			a[i] = (double*)_aligned_malloc(SIZE * sizeof(double), 16);
		double *b = (double*)_aligned_malloc(SIZE * sizeof(double), 16);
		
		double *x = new double[SIZE];
		int *ipvt = new int[SIZE];

		double ldaa = static_cast<double>(SIZE);
		double lda = ldaa + 1;
		double ops, norma, normx;
		double resid;
		int info;

		auto start = system_clock::now();
		tMain << "Timing attempt: " << k;

		// Main application
		initialise(a, b, ops, norma, lda);
		run(a, b, info, lda, SIZE, ipvt);
		validate(a, b, x, norma, normx, resid, lda, SIZE);

		auto end = system_clock::now();
		auto total = end - start;
		tMain << ", " << duration_cast<milliseconds>(total).count() << endl;

		// Free the memory
		// _aligned_free frees the memory allocated in the heap using C style.
		for (int i = 0; i < SIZE; ++i)
			_aligned_free(a[i]);
		delete[] a;
		_aligned_free(b);
		delete[] x;
		delete[] ipvt;
	}
	
	tDgesl.close();
	tDaxpy.close();
	tDgefa.close();
	tMain.close();
	tInitialise.close();
	tRun.close();
	tValidate.close();
	
	//system("Pause");
	return 0;
}