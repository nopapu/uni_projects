#include <iostream>
#include <chrono>
#include <fstream>

using namespace std;
using namespace std::chrono;

/*
This small project has been developed to demonstrate that the use of threads has a threshold from which starts being efficient.
*/

int main()
{
	double *b = new double[1000];

	for (int i = 0; i < 1000; ++i)
		b[i] = 0.0;
	
	for (int i = 0; i < 100; i++)
	{
		auto starts = system_clock::now();
		for (int i = 0; i < 1000; ++i)
			b[i] = 100 * i;
		auto ends = system_clock::now();
		auto totals = ends - starts;
		cout << "Sequential: " << duration_cast<milliseconds>(totals).count() << " milliseconds" << endl;

		auto startp = system_clock::now();
		#pragma omp parallel for num_threads(4)
		for (int i = 0; i < 1000; ++i)
			b[i] = 100 * i;
		auto endp = system_clock::now();
		auto totalp = endp - startp;
		cout << "Parallel: " << duration_cast<milliseconds>(totalp).count() << " milliseconds" << endl;
	}

	system("pause");
	delete[] b;
}